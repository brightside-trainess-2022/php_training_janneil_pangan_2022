<?php
require_once("../crud/html/header.html");
include_once("../crud/connection/connection.php");

$con =  connection();

$user = "";
$pass = "";
$gender = "";
$role = "";
$language = "";
$arrlanguage = [];
$comments = "";
$tc = "";

if (isset($_POST["bsubmit"])) {

    $ok = true;
    // $errors = [];

    if (!isset($_POST["user"]) || $_POST["user"] === '') {
        $ok = false;
        // array_push($errors,"Email")

    } else {
        $user = htmlspecialchars($_POST["user"] ?? "", ENT_QUOTES);
    };

    if (!isset($_POST["pass"]) || $_POST["pass"] === '') {
        $ok = false;
    } else {
        $pass = htmlspecialchars($_POST["pass"] ?? "", ENT_QUOTES);
    };

    if (!isset($_POST["gender"]) || $_POST["gender"] === '') {
        $ok = false;
    } else {
        $gender = htmlspecialchars($_POST["gender"] ?? "", ENT_QUOTES);
    };

    if (!isset($_POST["role"]) || $_POST["role"] === '') {
        $ok = false;
    } else {
        $role = htmlspecialchars($_POST["role"], ENT_QUOTES);
    };

    if (!isset($_POST["language"]) || !is_array($_POST["language"]) || count($_POST["language"]) === 0) {
        $ok = false;
    } else {
        $language = htmlspecialchars(implode(' ', $_POST["language"]), ENT_QUOTES);
        $arrlanguage = explode(' ', $language);
    };

    if (!isset($_POST["comments"]) || $_POST["comments"] === '') {
        $comments = "no comment";
    } else {
        $comments = htmlspecialchars($_POST["comments"], ENT_QUOTES);;;
    };

    if (!isset($_POST["tc"]) || $_POST["tc"] === '') {
        $ok = false;
    } else {
        $tc = htmlspecialchars($_POST["tc"], ENT_QUOTES);;
    };



    if ($ok) {
        echo  "REGISTRATION SUCCESS <br>";
        // echo  "Username: " .$user ."<br>";
        // echo  "Password: "  .$pass ."<br>";
        // echo  "gender: ". $gender  . "<br>";
        // echo  "role: ". $role. "<br>";
        // echo  " language: " .$language ."<br>";
        // echo  " Comments: ". $comments ."<br>";
        // echo  "tc :".$tc; 


        $hash = password_hash($pass, PASSWORD_DEFAULT);
        $sql = $con->prepare("INSERT INTO `users`(`user`, `pass`, `gender`, `language`, `comments`, `tc`, `role`) VALUES (?,?,?,?,?,?,?)");
        $sql->bind_param("sssssss", $user, $hash, $gender, $language, $comments, $tc, $role);
        $sql->execute();
    }
}
?>
<main>
    <form action="" method="post" enctype="multipart/form-data">
        Username: <input type="text" name="user" value="<?php echo htmlspecialchars($user, ENT_QUOTES); ?>" required>
        <br>
        Password: <input type="password" name="pass" value="<?php echo htmlspecialchars($pass, ENT_QUOTES); ?>" required>
        <br>
        Gender:
        <input type="radio" name="gender" value="M" <?php if ($gender == "M") {
                                                        echo ' checked';
                                                    } ?> required> Male
        <input type="radio" name="gender" value="F" <?php if ($gender == "F") {
                                                        echo ' checked';
                                                    } ?> required> Female
        <input type="radio" name="gender" value="O" <?php if ($gender == "O") {
                                                        echo ' checked';
                                                    } ?> required> Other
        <br>
        Role:
        <select name="role" id="" required>
            <option value="">Please Select</option>
            <option value="admin" <?php if ($role == "admin") {
                                        echo ' selected';
                                    } ?>> Admin</option>
            <option value="member" <?php if ($role == "member") {
                                        echo ' selected';
                                    } ?>> Member</option>

        </select>
        <br>
        Languages:
        <select multiple name="language[]" multiple size="3" id="" required>
            <option value="en" <?php if (in_array("en", $arrlanguage)) {
                                    echo ' selected';
                                } ?>>English</option>
            <option value="fr" <?php if (in_array("fr", $arrlanguage)) {
                                    echo ' selected';
                                } ?>>French</option>
            <option value="it" <?php if (in_array("it", $arrlanguage)) {
                                    echo ' selected';
                                } ?>>Italian</option>
        </select>
        <br>
        <br>
        Comment:
        <br>
        <textarea name="comments" id="" cols="30" rows="10"><?php echo htmlspecialchars($comments, ENT_QUOTES); ?></textarea>
        <br>
        I accept the T & C : <input type="checkbox" name="tc" value="ok" id="" required<?php
                                                                                        if ($tc === "ok") {
                                                                                            echo ' checked';
                                                                                        }
                                                                                        ?>><br>
        <input type="submit" name="bsubmit" value="Register">
        <br>
        <input accept="image/*" type="file" name="picture" id="picture">



    </form>
</main>

<?php
require_once("../crud/html/footer.html");
?>