<?php
require_once("../crud/html/header.html");
include_once("../crud/connection/connection.php");

$con =  connection();

$sql = "SELECT `user`,`gender`,`role`,`language`,`comments` FROM `users`";
$result = $con->query($sql);


?>
<main>
    <div class="table-database">
        <form action="" method="post">
            <table>
                <tr>
                    <th>m</th>
                    <th>Gender</th>
                    <th>Role</th>
                    <th>Language</th>
                    <th>Comments</th>
                </tr>
                <?php
                foreach ($result as $row) {
                ?>
                    <tr>
                        <?php
                        foreach ($row as $value) {
                            echo "<td>" . $value . "</td>";
                        }
                        ?>
                        <td><button>Update</button></td>
                        <td><button>Delete</button></td>
                    </tr>
                <?php } ?>
            </table>
        </form>
    </div>
</main>