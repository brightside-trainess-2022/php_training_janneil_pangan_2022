<?php

//function and include
include_once("../introphp/functions.php");

echo oper(3,2,"+")."<br>";
echo oper(3,2,"-")."<br>";
echo oper(3,2,"s")."<br>";

//global
$x = 200;
$y = 100;

function global1(){
    $GLOBALS["z"] = "global ".$GLOBALS["x"] + $GLOBALS["y"];

}

global1();
echo $z;
echo "<br>";

// print and echo statement

echo nl2br("hello world \r\n");
print "hello " . "earth" ."<br>";

// variables and datatypes
$name = "Neil";
$num = 28;
$dec = 1.50;
$is_allowed = true;

echo "My name is ".$name;
echo "<br>";

//condition and Operators (&&,||,!,xor)

$x = 10;
$y = 8;
$z = 4;

$num -= 7;
echo $num;
echo "<br>";
if($x > $y || $x == $y){
    echo $x." is higher or equal to ".$y;
}else{
    echo $y." is higher ".$x;
}
echo "<br>";
//spaceship
echo $z<=>$y;
echo "<br>";

$year = 2004;
if($year % 400 == 0 || $year % 4 ==0){
    echo $year." is leap year";
} else {
    echo $year." is not leap year";
}
echo "<br>";

// if($x == $y && "Hi" == "Hi"){
//     echo "true";
// }else{
//     echo "false";
// }
// echo "<br>";
// if($x == $y xor "Hi" == "Hi"){
//     echo "true";
// }else{
//     echo "false";
// }
// echo "<br>";
// echo $x++ ." vs ". ++$x;
// echo "<br>";

$rating = "F";
switch($rating){
    case "A" :
        echo "Excellent";
        break;
    case "B" :
        echo "Very Good";
        break;
    case "C" :
        echo "Good";
        break;
    default:
        echo "No rating";
}
echo "<br>";

// array and loops

$animals = ["dog","cat","pig","bird"];
$asso = ["A" => "Excellent", "B"=>"Very Good", "C"=>"Good"];
$cars = ["Expensive" =>["Audi","Mercedez"],"Affordable"=>["Ford", "Toyoya"]];
$car2 = [["Audi","Mercedez"],["Ford", "Toyoya"]];
// $test = ["4" => "one", "tw" => "two","three",4];

echo $cars["Expensive"][0]." is " .$asso["A"];
echo "<br>";

for($i = 1; $i<= 5; $i++){
    echo $i ."<br>";
}

foreach($asso as $grade => $rating){
    echo $grade."=".$rating ."<br>";
}

foreach($cars as $price => $brands){
    foreach($brands as $brand){
        echo $brand." is ".$price ."<br>";
    } 
}

// foreach($cars as $brands){
//     foreach($brands as $brand){
//         echo $brand."<br>";
//     } 
// }


// $n = 1;

// while($n <= 10){
//     echo $n ."<br>";
//     $n++;
// }

// do{
//     echo $n ."<br>";
//     $n++;;
// } while($n < 1)


?>
