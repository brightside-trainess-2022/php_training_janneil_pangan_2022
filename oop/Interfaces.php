<?php

// Interface Electricity{
//     public function voltage();
//     public function elecord();
//     public function outlet();
// }

abstract class Electricity{
    abstract function voltage();
    abstract function elecord();
    abstract function outlet();
    public function powerOn(){}
    public function powerOff(){}
}



class Television extends Electricity{
    public function changeChannel(){


    }
    public function adjustVolume(){

    }

    public function voltage(){}
    public function elecord(){}
    public function outlet(){}
}

// interface lets the programmer know that they need certain methods to make their class complete
// interface use implements for abstract user extends abstract can't be instantiated abstract is interface + class
?>