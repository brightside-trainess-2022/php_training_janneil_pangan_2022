<?php


class Person{

    const AVG_LIFE_SPAN = 80;
    private $firstName;
    private $lastName;
    private $yearBorn;

    function __construct($tempFirst = "",$tempLast = "",$tempYear= ""){

        echo "Person Constructor".PHP_EOL;
        $this->firstName = $tempFirst;
        $this->lastName = $tempLast;
        $this->yearBorn = $tempYear;
    }

    public function getFirstName(){
        return $this->firstName;
    }

    public function setFirstName($tempName){
        $this->firstName = $tempName;
    }

    protected function getFullName(){
        echo "Person->getFullName".PHP_EOL;

        return $this->firstName." ".$this->lastName.PHP_EOL;
    }

}
class Author extends Person{

    public static $centuryPopular = "19th";
    private $penName;

    function __construct($tempFirst = "",$tempLast = "",$tempYear= "",$tempPenName =""){
        echo "Author Constructor".PHP_EOL;

        Parent::__construct($tempFirst,$tempLast,$tempYear);

        $this->penName = $tempPenName;
    }

    public function getPenName(){
        return $this->perName.PHP_EOL;
    }
    public function getCompleteName(){
        echo "Author->getCompleteName".PHP_EOL;

        return $this->getFullName()  ." ".$this->penName.PHP_EOL;
    }
    public static function getCentury(){
        return self::$centuryPopular;
    }

    function __destruct(){
        echo "destruct -".$this->penName;
    }

    public function getFirstName(){
        return $this->penName;
    }
}
$newAuthor = new Author("Samuel Langhorne", "Clements", 1899, "Mark Twain");
// echo $newAuthor->getFirstName();
echo $newAuthor->getCompleteName();



// constructor runs at the creation of a class object
// destructor are executed right before an object is deleted cant take any parameters can be used to edit database
// Garbage cleanup(script ends) process trigger destructors always last
// Final Keyword function in our class to never change or overwritten by a child class it stop to inherit
?>

